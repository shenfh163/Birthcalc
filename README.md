#Birthcalc

#简介
八字算命，也称为四柱算命，是以一个人出生的年、月、日、时为基点，按照我国传统的天干地支时辰计数方法排出当时的天干与地支就构成了一个八字组合，也称为生辰八字算命法，八字算命预测准确，范围广泛，是在民间影响最大，预测面最广的预测方法。

#演示
http://www.wandoujia.com/apps/com.mj.birthcalc

#捐赠
开源，我们是认真的，感谢您对我们开源力量的鼓励。


![支付宝](https://git.oschina.net/uploads/images/2017/0607/164544_ef822cc0_395618.png "感谢您对我们开源力量的鼓励")
![微信](https://git.oschina.net/uploads/images/2017/0607/164843_878b9b7f_395618.png "感谢您对我们开源力量的鼓励")
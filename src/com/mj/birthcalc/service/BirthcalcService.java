package com.mj.birthcalc.service;

import java.util.LinkedHashMap;
import java.util.Map;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.mj.birthcalc.util.HttpUtils;
import com.mj.birthcalc.util.RegUtil;

/**
 * 生辰八字算命
 * @author zhaominglei
 * @date 2015-4-10
 * 
 */
public class BirthcalcService extends BaseService {
	@SuppressWarnings("unused")
	private static final String TAG = BirthcalcService.class.getSimpleName();
	private static final String M_SHEUP_BAZISUANMING_URL = "http://m.sheup.com/bazisuanming.php"; //三藏算命网手机版
	@SuppressWarnings("serial")
	public Map<String, String> typeMap = new LinkedHashMap<String, String>() {
		{
			put("公历", "1");
			put("农历", "2");
		}
	};
	@SuppressWarnings("serial")
	public Map<String, String> hourMap = new LinkedHashMap<String, String>() {
		{
			put("子 00:00-00:59", "0");
			put("丑 01:00-01:59", "1");
			put("丑 02:00-02:59", "2");
			put("寅 03:00-03:59", "3");
			put("寅 04:00-04:59", "4");
			put("卯 05:00-05:59", "5");
			put("卯 06:00-06:59", "6");
			put("辰 07:00-07:59", "7");
			put("辰 08:00-08:59", "8");
			put("巳 09:00-09:59", "9");
			put("巳 10:00-10:59", "10");
			put("午 11:00-11:59", "11");
			put("午 12:00-12:59", "12");
			put("未 13:00-13:59", "13");
			put("未 14:00-14:59", "14");
			put("申 15:00-15:59", "15");
			put("申 16:00-16:59", "16");
			put("酉 17:00-17:59", "17");
			put("酉 18:00-18:59", "18");
			put("戌 19:00-19:59", "19");
			put("戌 20:00-20:59", "20");
			put("亥 21:00-21:59", "21");
			put("亥 22:00-22:59", "22");
			put("子 23:00-23:59", "23");
		}
	};
	
	public String getBirthcalcInfo(String type, String year, String month, String day, String hour) {
		if (type == null || type.equals("") || year == null || year.equals("")
				|| month == null || month.equals("") || day == null || day.equals("")
				|| hour == null || hour.equals("")) {
			return null;
		}
		String html = calc(type, year, month, day, hour);
		if (html != null && !html.equals("")) {
			html = html.replaceAll(RegUtil.getMatchRegStr(html, "<div class=\"list_more\">(.+?)</div>"), "");
			Document document = Jsoup.parse(html);
			Elements elements = document.getElementsByClass("sanzang_subs");
			if (elements != null && !elements.isEmpty()) {
				StringBuilder result = new StringBuilder();
				int i = 0;
				for (Element element : elements) {
					String content = element.html();
					
					if (i < 4) {
						result.append(content);
					}
					i++;
				}
				return result.toString();
			}
		}
		
		return null;
	}
	
	public String calc(String type, String year, String month, String day, String hour) {
		StringBuilder param = new StringBuilder();
		param.append("t=").append(typeMap.get(type));
		param.append("&y=").append(year);
		param.append("&m=").append(month);
		param.append("&d=").append(day);
		param.append("&s=").append(hourMap.get(hour));
		param.append("&sizhusuanming=%C9%FA%B3%BD%B0%CB%D7%D6%CB%E3%C3%FC");
		String html = HttpUtils.doPostForMSheup(M_SHEUP_BAZISUANMING_URL, param.toString(), "gbk");
		return html;	
	}
}
